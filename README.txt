Assignment 2 - Questions

1.How are Views and ViewGroups different?
 Views are the basic building block for user interface(UI) components.
 A ViewGroup is the base class for layouts and views containers. ViewGroup is a view that can contain other views (called children.)

2. Explain how the layout_gravity and gravity attributes of a View works.
	Gravity manages how the contents of the View Group is positioned. 
	Layout_gravity sets the gravity of the View or Layout from its parent. 
	
3. Explain how the weight attribute works.
	The weight attribute assigns an importance value to a view. 

4. How is the Model-View-Presenter pattern different than the Model-View-Controller
pattern?
	MVP pattern is responsible for putting the model together to the view vs MVC the
	controller is based on behaviors and can be shared across views.  

5. Explain the difference between a Local Unit Test and an Instrumented Unit Test?
	Local Unit test runs on your machine’s local JVM which is usually faster. 
	Instrumented Unit tests this runs on hardware or an emulator. 

6. Explain how Android chooses which layout to inflate when a device is in portrait or
landscape mode.
	The layout will hold all the XML layouts for portrait mode.
	The layout-land will hold all the XML layouts for landscape mode.