package bayeh1.todoapp;

import org.junit.Test;
import org.mockito.Mock;

import bayeh1.todoapp.interfaces.IModel;
import bayeh1.todoapp.interfaces.IPresenter;
import bayeh1.todoapp.interfaces.IView;
import bayeh1.todoapp.presenter.Presenter;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by bayeh on 2/21/17.
 */

public class MainPresenterTests {
    IModel model;
    IView view;


    @Test
    public void TestNextBtnCallsMethodOnPresenter(){
        view = mock(IView.class);
        //model = mock(IModel.class);
        IPresenter presenter = new Presenter(model,view);
        presenter.NextTodo();
        verify(view).navToNextBtn();

    }
//    @Test
//    public void TestPrevBtnCallMethodOnPresenter(){
//
//    }
}
