package bayeh1.todoapp.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import bayeh1.todoapp.Constants;
import bayeh1.todoapp.R;
import bayeh1.todoapp.TodoAdapter;
import bayeh1.todoapp.interfaces.IPresenter;
import bayeh1.todoapp.interfaces.IView;
import bayeh1.todoapp.interfaces.Service;
import bayeh1.todoapp.models.Todo;
import bayeh1.todoapp.models.TodoModel;
import bayeh1.todoapp.presenter.Presenter;
import bayeh1.todoapp.services.TodoService;


public class MainActivity extends AppCompatActivity implements IView, View.OnClickListener {

    //private static final String TAG = MainActivity.class.getSimpleName();

    // the ePresenter
    IPresenter presenter;
    TodoAdapter adapter;

    RecyclerView mRecyclerView;

    Button prev_btn;
    Button next_btn;
    Button add_btn;
    Button edit_btn;
    TextView TodoItem;
    TextView TodoSubItem;
    TextView date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set the main layout
        setContentView(R.layout.activity_main);
        bindView();
        presenter = new Presenter(new TodoModel(new TodoService()),this);
        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        Service service = TodoService.getInstance();
        presenter = new Presenter(new TodoModel(service),this);
        adapter = new TodoAdapter(presenter);
        adapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(adapter);



    }

    public void setPresenter(IPresenter presenter){
        this.presenter = presenter;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Grabs references to all needed widgets and sets click listeners.
     */
    private void bindView() {
        TodoItem = (TextView) findViewById(R.id.Todos);
        TodoSubItem = (TextView) findViewById(R.id.SubTodo);
        date = (TextView)findViewById(R.id.date);
        prev_btn = (Button) findViewById(R.id.prevBtn);
        next_btn = (Button) findViewById(R.id.nextBtn);
        add_btn = (Button) findViewById(R.id.addBtn);
        edit_btn = (Button) findViewById(R.id.edit_btn);

        prev_btn.setOnClickListener(this);
        next_btn.setOnClickListener(this);
        add_btn.setOnClickListener(this);
        edit_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.prevBtn:
                presenter.PrevTodo();
                break;
            case R.id.nextBtn:
                presenter.NextTodo();
                break;
            case R.id.addBtn:
                presenter.ShowAddEDITView(null);
                break;
            case R.id.edit_btn:
                presenter.ShowAddEDITView(presenter.getCurrentTodo());
                break;
        }

    }
    @Override
    public void displayTodo(Todo todo) {
        if(todo == null){
            TodoItem.setText("No Task Available");
            return;
        }
        if(todo.isImportant()) {
            TodoItem.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.important_text_size));
        } else {
            TodoItem.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.normal_text_size));
        }
        TodoItem.setText(todo.getTask());
        TodoSubItem.setText(todo.getSubTask());
        date.setText(todo.getDate());


    }

    @Override
    public void displayNextTodo(Todo todo) {

    }

    @Override
    public void showAddView() {
        Intent intent = AddActivityView.getIntent(this);
        intent.putExtra("START_REASON",Constants.ADD_TODO_REQUEST_CODE);
        startActivityForResult(intent, Constants.ADD_TODO_REQUEST_CODE);

    }
    @Override
    public void showEditView(Todo todo) {
        Intent in = AddActivityView.getIntent(this);
        in.putExtra("TODO",presenter.getCurrentTodo().getTask());
        in.putExtra("SUBTODO",presenter.getCurrentTodo().getSubTask());
        in.putExtra("DUEDATE",presenter.getCurrentTodo().getDate());
        in.putExtra("START_REASON",Constants.EDIT_TODO_REQ_CODE);
        startActivityForResult(in,Constants.EDIT_TODO_REQ_CODE);


    }

    //IDK the use for onActivity
    //when it returns is handles the result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == Constants.ADD_TODO_REQUEST_CODE){
                presenter.handleSaveAddTodoReq(data.getStringExtra("TODO"),data.getStringExtra("SUBTODO"),data.getStringExtra("DUEDATE"));
                handleAdd(0);

            }
            if(requestCode == Constants.EDIT_TODO_REQ_CODE){
                presenter.handleSaveAddTodoReq(data.getStringExtra("TODO"),data.getStringExtra("SUBTODO"),data.getStringExtra("DUEDATE"));
                handleEdit();
            }
           // Log.d(TAG, data.getStringExtra("TODO")+" ")
        }
    }


    @Override
    public void navToNextBtn() {


    }


    @Override
    public void handleDelete(int position) {
        adapter.notifyItemRemoved(position);

    }

    @Override
    public void handleMove(int adapterPosition, int newPos) {
        adapter.notifyItemMoved(adapterPosition,newPos);
    }

    @Override
    public void handleAdd(int i) {
        adapter.notifyItemInserted(i);
        mRecyclerView.scrollToPosition(0);

    }

    @Override
    public void handleEdit() {
        adapter.notifyDataSetChanged();
        mRecyclerView.scrollToPosition(0);
    }


}
