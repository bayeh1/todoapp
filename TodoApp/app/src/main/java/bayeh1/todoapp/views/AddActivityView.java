package bayeh1.todoapp.views;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import bayeh1.todoapp.Constants;
import bayeh1.todoapp.EditTodoViewModel;
import bayeh1.todoapp.R;
import bayeh1.todoapp.interfaces.IEditPresenter;
import bayeh1.todoapp.interfaces.IEditView;
import bayeh1.todoapp.interfaces.IPresenter;
import bayeh1.todoapp.models.TodoModel;
import bayeh1.todoapp.presenter.EditTaskPresenter;
import bayeh1.todoapp.services.TodoService;

public class AddActivityView extends AppCompatActivity implements IEditView, View.OnClickListener {

    // the ePresenter
    IEditPresenter ePresenter;


    TextView titleVW;
    TextView TodoItem;
    TextView TodoSubItem;
    TextView dueDateTV;
    EditText editTodoItem;
    EditText editSubItem;
    EditText editDueDate;
    Button saveBtn;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_view);
        bindView();
        ePresenter = new EditTaskPresenter(new TodoModel(new TodoService()),this);
        ePresenter.showTask(getIntent().getIntExtra("START_REASON",Constants.ADD_TODO_REQUEST_CODE));
        ePresenter.setViewModel(getIntent());
        //editTodoItem.setText(getIntent().getStringExtra("TODO"));
        //editSubItem.setText(getIntent().getStringExtra("SUBTODO"));

    }



    private void bindView() {
        titleVW = (TextView)findViewById(R.id.title_view);
        TodoItem = (TextView)findViewById(R.id.addTodoText);
        dueDateTV = (TextView)findViewById(R.id.dueDateTV);
        TodoSubItem = (TextView)findViewById(R.id.AddLocationText);
        editTodoItem = (EditText)findViewById(R.id.editNewTask);
        editSubItem = (EditText)findViewById(R.id.editNewLocation);
        editDueDate = (EditText)findViewById(R.id.editNewDueDate);
        saveBtn = (Button)findViewById(R.id.saveBtn);

        saveBtn.setOnClickListener(this);
        editTodoItem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ePresenter.updateTask(s.toString());

            }
        });
        editSubItem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ePresenter.updateLocation(s.toString());

            }
        });
        editDueDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ePresenter.updateDueDate(s.toString());

            }
        });

    }

    public static Intent getIntent(Context context){
        //explicit intent
        Intent intent = new Intent(context,AddActivityView.class);
        return intent;


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.saveBtn:
                //ePresenter.handleAddTodoReq();
                ePresenter.validateModel();
                ePresenter.saveTask(editTodoItem.getText().toString(),editSubItem.getText().toString(),editDueDate.getText().toString());
                break;

        }
    }

    @Override
    public void displayTask(int task) {
        titleVW.setText(task);

    }

    @Override
    public void updateViewWithViewModel(EditTodoViewModel vm) {
        //TodoItem.setText("TODO: " + vm.todo + ", SubTodo: " + vm.subTodo);
    }

    @Override
    public void returnResult(EditTodoViewModel vm) {
        Intent intent = vm.makeIntent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void displayInvalid(EditTodoViewModel vm) {
        //TodoItem.setText("View model is not valid");

    }

    @Override
    public void setInitialFields(EditTodoViewModel vm) {
        editTodoItem.setText(vm.todo);
        editSubItem.setText(vm.subTodo);
        editDueDate.setText(vm.dueDate);
    }

    @Override
    public void sendTodoBack(Intent intent) {
        setResult(RESULT_OK, intent);
        finish();

    }
}
