package bayeh1.todoapp.services;

import java.util.ArrayList;
import java.util.List;

import bayeh1.todoapp.interfaces.Service;
import bayeh1.todoapp.models.Todo;

/**
 * Created by bayeh on 2/13/17.
 */

public class TodoService implements Service {

    private static TodoService ourInstance = new TodoService();

    public static TodoService getInstance() {
        return ourInstance;
    }

    List<Todo> todos;

    public TodoService(){
        todos = new ArrayList<>();
        Todo todo = new Todo("Buy Food", "At Giant","3/10/2017",false);
        todos.add(todo);
        todo = new Todo("GO TO THE GYM ","L.A Fitness","Everyday",false);
        todos.add(todo);
        todo = new Todo("Pick up dry cleaning ","Timonium Shopping center","4/4/2017",false);
        todos.add(todo);
        todo = new Todo("Take dog to the vet! ","Timonium Vet Center","5/14/2017",false);
        todos.add(todo);
        todo = new Todo("Yoga","L.A Fitness","Every Tuesday",false);
        todos.add(todo);
        todo = new Todo("Grab donuts","At Crispy Creme","5/22/2017",false);
        todos.add(todo);
        todo = new Todo("Take cat to the vet! ","Timonium Vet Center","5/14/2017",false);
        todos.add(todo);
        todo = new Todo("Pick up mom form air port ","BWI","4/4/2017",false);
        todos.add(todo);
        todo = new Todo("Pick up basketball game","City Park Courts","5/11/2017",false);
        todos.add(todo);
        todo = new Todo("Take hamster to the vet! ","Timonium Vet Center","5/14/2017",false);
        todos.add(todo);


    }

    @Override
    public List<Todo> getTodos() {
        return todos;
    }
}
