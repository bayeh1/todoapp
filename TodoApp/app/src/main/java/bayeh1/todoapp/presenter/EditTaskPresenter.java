package bayeh1.todoapp.presenter;

import android.content.Intent;

import bayeh1.todoapp.Constants;
import bayeh1.todoapp.EditTodoViewModel;
import bayeh1.todoapp.R;
import bayeh1.todoapp.interfaces.IEditPresenter;
import bayeh1.todoapp.interfaces.IEditView;
import bayeh1.todoapp.interfaces.IModel;
import bayeh1.todoapp.interfaces.IView;
import bayeh1.todoapp.models.Todo;
import bayeh1.todoapp.models.TodoModel;
import bayeh1.todoapp.views.AddActivityView;

/**
 * Created by bayeh on 2/23/17.
 */

public class EditTaskPresenter implements IEditPresenter {
    IModel model;
    IEditView view;
    IView view1;
    EditTodoViewModel viewModel;
    public EditTaskPresenter(IModel model, IEditView view){
        this.model = model;
        this.view = view;
        this.viewModel = new EditTodoViewModel();

    }


    @Override
    public void showTask(int requestCode) {
        if(requestCode == Constants.ADD_TODO_REQUEST_CODE) {
            view.displayTask(R.string.add_new_task);
        } else {
            view.displayTask(R.string.edit_task);
        }
    }

    @Override
    public void setViewModel(Intent intent) {
        if(intent.getIntExtra("START_REASON", Constants.ADD_TODO_REQUEST_CODE) == Constants.EDIT_TODO_REQ_CODE){
            viewModel.todo = intent.getStringExtra("TODO");
            viewModel.subTodo = intent.getStringExtra("SUBTODO");
            viewModel.dueDate = intent.getStringExtra("DUEDATE");
            view.updateViewWithViewModel(viewModel);
            view.setInitialFields(viewModel);
        }


    }

    @Override
    public void updateTask(String text) {
        viewModel.todo = text;
        view.updateViewWithViewModel(viewModel);

    }

    @Override
    public void updateLocation(String text) {
        viewModel.subTodo = text;
        view.updateViewWithViewModel(viewModel);


    }

    @Override
    public void updateDueDate(String text) {
        viewModel.dueDate = text;
        view.updateViewWithViewModel(viewModel);

    }

    @Override
    public void validateModel() {
        if(viewModel.validate()){
            view.returnResult(viewModel);
        } else {
            view.displayInvalid(viewModel);
        }

    }

    @Override
    public void saveTask(String todo, String subTodo, String dueDate) {
        Intent intent = new Intent();
        intent.putExtra("TODO",todo);
        intent.putExtra("SUBTODO",subTodo);
        intent.putExtra("DUEDATE",dueDate);
        view.sendTodoBack(intent);

    }

    @Override
    public void handleAddTodoReq() {
        model.add(new Todo("Stuff","subStuf","some date",false));
        view1.handleAdd(0);
    }

}
