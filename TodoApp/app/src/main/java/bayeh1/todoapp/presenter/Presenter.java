package bayeh1.todoapp.presenter;

import java.util.List;

import bayeh1.todoapp.interfaces.IPresenter;
import bayeh1.todoapp.interfaces.IModel;
import bayeh1.todoapp.interfaces.IView;
import bayeh1.todoapp.models.Todo;

/**
 * Created by bayeh on 2/13/17.
 */
;

public class Presenter implements IPresenter {
    IModel model;
    IView view;
    int currentTaskIndex;
   // public Presenter(){

    public Presenter(IModel model,IView view){

        this.model = model;
        this.view = view;
        currentTaskIndex = 0;
      view.displayTodo(getCurrentTodo());

    }

    @Override
    public void PrevTodo() {
        List<Todo> songs = model.getTodos();
        currentTaskIndex--;
        if(currentTaskIndex <= 0){
            currentTaskIndex = 0;
        }
        view.displayTodo(getCurrentTodo());

    }

    @Override
    public void NextTodo() {
        List<Todo> todos = model.getTodos();
        currentTaskIndex++;
        if(currentTaskIndex >= todos.size()){
            currentTaskIndex = 0;
        }
        view.displayTodo(getCurrentTodo());

    }

    @Override
    public Todo getCurrentTodo() {
        List<Todo> todos = model.getTodos();
        if(todos.size() == 0) {
            return null;
        }
        return todos.get(currentTaskIndex);

    }

    @Override
    public void markTodo() {
//        Todo todo = getCurrentTodo();
//        todo.setImportant(!todo.isImportant());
//        view.displayTodo(todo);

    }

    @Override
    public Todo getNextTodo() {
        return null;
    }

    @Override
    public List<Todo> getTodos() {
        return model.getTodos();
    }

    @Override
    public void handleClick(Todo todo, int adapterPos) {
        currentTaskIndex = adapterPos;
        ShowAddEDITView(todo);

    }


    @Override
    public void handleLongPress(int position) {
        model.delete(position);
        view.handleDelete(position);

    }

    @Override
    public void ShowAddEDITView(Todo todo) {
        if (todo == null){
            view.showAddView();
        }else {
            view.showEditView(todo);
        }
    }

    @Override
    public void handleSaveAddTodoReq(String todo, String subTodo,String date) {
        List<Todo> todos = model.getTodos();
        Todo task = getCurrentTodo();
        task.setTask(todo);
        task.setSubTask(subTodo);
        task.setDate(date);


    }

    @Override
    public void saveEditTodoReq() {

    }


}
