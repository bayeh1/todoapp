package bayeh1.todoapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.content.Intent;

import bayeh1.todoapp.interfaces.IPresenter;
import bayeh1.todoapp.interfaces.IView;
import bayeh1.todoapp.models.Todo;
import bayeh1.todoapp.presenter.Presenter;

/**
 * Created by bayeh on 3/5/17.
 */
public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.TodoViewHandler>{
    IPresenter presenter;
    IView view;

    public TodoAdapter(IPresenter presenter){
        this.presenter = presenter;
    }


    @Override
    public TodoViewHandler onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_list_item, parent,false);
        return new TodoViewHandler(view);
    }

    @Override
    public void onBindViewHolder(TodoViewHandler holder, int position) {
        Todo task = presenter.getTodos().get(position);
        holder.bind(task);
    }

    @Override
    public int getItemCount() {
        return presenter.getTodos().size();
    }

    class TodoViewHandler extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        TextView textView;
        TextView textView2;
        TextView date;
        Todo task;

        public TodoViewHandler(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.listItemTextView);
            textView2 = (TextView) itemView.findViewById(R.id.listItemTextView2);
            date = (TextView) itemView.findViewById(R.id.listItemDueDate);
        }

        public void bind(Todo todo){
            task = todo;
            textView.setText(todo.getTask());
            textView2.setText(todo.getSubTask());
            date.setText(todo.getDate());
            textView.setOnClickListener(this);
            textView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.listItemTextView:
                    // Handles click to edit view
                    presenter.handleClick(task,getAdapterPosition());
                    break;
            }
        }

        @Override
        public boolean onLongClick(View v) {
            switch (v.getId()){
                case R.id.listItemTextView:
                    presenter.handleLongPress(getAdapterPosition());
            }
            return false;
        }

    }
}
