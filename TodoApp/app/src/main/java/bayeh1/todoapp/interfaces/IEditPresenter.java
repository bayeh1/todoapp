package bayeh1.todoapp.interfaces;

import android.content.Intent;

/**
 * Created by bayeh on 2/23/17.
 */

public interface IEditPresenter {
    void showTask(int requestCode);
    void setViewModel(Intent intent);
    void updateTask(String text);
    void updateLocation(String text);
    void updateDueDate(String text);
    void validateModel();
    void saveTask(String todo, String subTodo, String dueDate);
    void handleAddTodoReq();


}
