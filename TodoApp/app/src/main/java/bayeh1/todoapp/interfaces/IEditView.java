package bayeh1.todoapp.interfaces;

import android.content.Intent;

import bayeh1.todoapp.EditTodoViewModel;

/**
 * Created by bayeh on 2/23/17.
 */

public interface IEditView {
    void displayTask (int task);
    void updateViewWithViewModel(EditTodoViewModel vm);

    void returnResult(EditTodoViewModel viewModel);

    void displayInvalid(EditTodoViewModel viewModel);

    void setInitialFields(EditTodoViewModel viewModel);

    void sendTodoBack(Intent intent);

}
