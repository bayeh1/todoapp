package bayeh1.todoapp.interfaces;

/**
 * Created by bayeh on 2/13/17.
 */
import java.util.*;
import bayeh1.todoapp.models.Todo;

public interface IModel {
    List<Todo> getTodos();

    void delete(int position);

    int move(int adapterPosition);

    void move(int from, int to);

    void add(Todo t);
}
