package bayeh1.todoapp.interfaces;
import java.util.*;
import bayeh1.todoapp.models.Todo;
/**
 * Created by bayeh on 2/13/17.
 */

public interface Service {
    List<Todo> getTodos();
}
