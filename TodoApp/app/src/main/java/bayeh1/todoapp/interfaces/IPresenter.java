package bayeh1.todoapp.interfaces;
import java.util.List;

import bayeh1.todoapp.models.Todo;
/**
 * Created by bayeh on 2/13/17.
 */

public interface IPresenter {
    void PrevTodo();
    void NextTodo();
    Todo getCurrentTodo();
    void markTodo();
    Todo getNextTodo();
    List<Todo> getTodos();
    void handleClick(Todo todo,int adapterPos);
    void handleLongPress(int position);


    void ShowAddEDITView(Todo todo);

    void handleSaveAddTodoReq(String todo, String subTodo, String date);

    void saveEditTodoReq();
}
