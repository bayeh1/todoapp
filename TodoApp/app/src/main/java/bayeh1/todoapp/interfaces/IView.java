package bayeh1.todoapp.interfaces;

import bayeh1.todoapp.models.Todo;

/**
 * Created by bayeh on 2/13/17.
 */

public interface IView {
    void displayTodo(Todo todo);
    void displayNextTodo (Todo todo);
    void showAddView();
    void showEditView(Todo todo);
    void navToNextBtn();
    void handleDelete(int position);
    void handleMove(int adapterPosition, int newPos);
    void handleAdd(int i);
    void handleEdit();
}
