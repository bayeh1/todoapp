package bayeh1.todoapp.models;

/**
 * Created by bayeh on 2/13/17.
 */

public class Todo {
    private String task;
    private String subTask;
    private String date;
    private boolean isImportant;

    public Todo() {}
    public Todo(String task, String subTask, String date,boolean important){
        this.task = task;
        this.subTask = subTask ;
        this.date = date;
        this.isImportant = important;
    }

    public String toString() {
        return task;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getSubTask() {
        return subTask;
    }

    public void setSubTask(String subTask) {
        this.subTask = subTask;
    }
    public String getDate(){
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }




    public boolean isImportant() {
        return isImportant;
    }

    public void setImportant(boolean important) {
        isImportant = important;
    }
}