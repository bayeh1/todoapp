package bayeh1.todoapp.models;
import java.util.*;

import bayeh1.todoapp.interfaces.IModel;
import bayeh1.todoapp.interfaces.Service;
import bayeh1.todoapp.services.TodoService;

/**
 * Created by bayeh on 2/13/17.
 */

public class TodoModel implements IModel {
    List<Todo> todos;

    public TodoModel(Service todoService) {
        todos = new ArrayList<>();
        todos.add(new Todo());
        todos = todoService.getTodos();
    }

    @Override
    public List<Todo> getTodos() {
        return todos;
    }

    @Override
    public void delete(int position) {
        todos.remove(position);

    }

    @Override
    public int move(int adapterPosition) {
        Todo item  = todos.remove(adapterPosition);
        todos.add(7,item);

        return 7;
    }

    @Override
    public void move(int from, int to) {
        Todo item = todos.remove(from);
        todos.add(to,item);

    }

    @Override
    public void add(Todo t) {
        todos.add(0,t);

    }
}
