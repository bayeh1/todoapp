package bayeh1.todoapp;

import android.content.Intent;

/**
 * Created by bayeh on 2/23/17.
 */
public class EditTodoViewModel {
    public String todo;
    public String subTodo;
    public String dueDate;

    public boolean validate(){
        return todo != null && subTodo != null && dueDate !=null;
    }

    public Intent makeIntent(){
        Intent intent = new Intent();
        intent.putExtra("TODO",todo);
        intent.putExtra("SUBTODO",subTodo);
        intent.putExtra("DUEDATE",dueDate);
        return intent;
    }
}
