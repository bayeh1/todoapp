package bayeh1.todoapp;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;


import bayeh1.todoapp.interfaces.IPresenter;
import bayeh1.todoapp.interfaces.IView;
import bayeh1.todoapp.views.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by bayeh on 2/21/17.
 */

@RunWith(AndroidJUnit4.class)
public class MainViewTests {
    @Rule
    public ActivityTestRule<MainActivity> mActivity = new ActivityTestRule<>(MainActivity.class);

    @Mock
    IPresenter presenter;

    @Before
    public void setup(){
        initMocks(this);
    }

    @Test
    public void TestNextBtnCallsMethodOnPresenter() {

        MainActivity activity = mActivity.getActivity();
        activity.setPresenter(presenter);
        //IView view = mActivity.getActivity();
        onView(withId(R.id.nextBtn)).perform(click());
        verify(presenter).NextTodo();

    }
    @Test
    public void TestPrevBtnCallMethodOnPresenter(){
        MainActivity activity = mActivity.getActivity();
        activity.setPresenter(presenter);
        onView(withId(R.id.prevBtn)).perform(click());
        verify(presenter).PrevTodo();
    }
}





